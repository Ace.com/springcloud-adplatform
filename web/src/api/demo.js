import request from '@/assets/util/request.js'
import {MENUID, URL} from "../assets/constants/constant-common";

export function addDemo(formData) {
  return request({
    url: URL+MENUID.addDemo,
    method: 'post',
    data: formData
  })
}
export function editDemo(formData) {
  return request({
    url: URL+MENUID.editDemo,
    method: 'post',
    data: formData
  })
}
