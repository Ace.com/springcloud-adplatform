package com.wwmxd.dao;

import com.wwmxd.entity.Demo;
import com.wwmxd.common.mapper.SuperMapper;

/**
 *
 * 
 *
 * @author WWMXD
 * @email 309980030@qq.com
 * @date 2018-01-12 10:59:07
 */
public interface DemoDao extends SuperMapper<Demo> {

}