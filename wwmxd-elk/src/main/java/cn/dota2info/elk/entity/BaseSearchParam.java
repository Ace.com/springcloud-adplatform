package cn.dota2info.elk.entity;


import lombok.Data;

/**
 * 基础查询类
 */
@Data
public class BaseSearchParam {
   private String name;
   private Object searchValue;
   private Integer current;
   private Integer size;
   private Integer count;

   public BaseSearchParam() {
   }
}
